<?php

declare(strict_types=1);

return [
    'country' => 'País',
    'state' => 'Provincia',
    'postal-code' => 'Código postal',
    'location' => 'Localidad',
    'address' => 'Dirección',
    'name' => 'Nombre para identificar esta dirección',
    'person_name' => 'Nombre',
    'person_name_placeholder' => 'Nombre de la persona que recibirá los pedidos',
    'person_surname_placeholder' => 'Apellidos de la persona que recibirá los pedidos',
    'person_surname' => 'Apellidos',
    'person_phone' => 'Teléfono de contacto',
    'name-placeholder' => 'Por ejemplo casa, trabajo, ...',
    'main' => 'Principal',
    'shipping-address' => 'Dirección de envío',
    'billing-address' => 'Dirección de facturación',
    'person_nif' => 'DNI/CIF/NIE',
    'person_nif_help' => 'Nº identificación fiscal para facturas',
];
