<?php

declare(strict_types=1);

return [
    'country' => 'País',
    'state' => 'Província',
    'postal-code' => 'Código postal',
    'location' => 'Localização',
    'address' => 'Endereço',
    'name' => 'Nome para identificar este endereço',
    'person_name' => 'Nome',
    'person_surname' => 'Apelidos',
    'name-placeholder' => 'Por exemplo casa, trabalho, ...',
    'main' => 'Principal',
    'shipping-address' => 'Endereço para envio',
    'billing-address' => 'Endereço de cobrança',
];
