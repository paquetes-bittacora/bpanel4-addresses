<?php /** @var \Bittacora\Bpanel4\Addresses\Models\ModelAddress $address */?>

<div class="display-address">
    @if($showName)
        @if($address->getName() !== '')
            <div>
                <div class="display-address-label">Nombre para identificar esta dirección:</div>
                <div>{{ $address->getName() }}</div>
            </div>
        @endif
    @endif

    @if($address->getPersonName() !== '')
        <div>
            <div class="display-address-label">Nombre:</div>
            <div>{{ $address->getPersonName() }}</div>
        </div>
    @endif
    @if($address->getPersonSurname() !== '')
        <div>
            <div class="display-address-label">Apellidos:</div>
            <div>{{ $address->getPersonSurname() }}</div>
        </div>
    @endif
    @if($address->getNif() !== null)
        <div>
            <div class="display-address-label">DNI/NIF/NIE:</div>
            <div>{{ $address->getNif() }}</div>
        </div>
    @endif
    @if($address->getPersonPhone() !== '')
        <div>
            <div class="display-address-label">Teléfono:</div>
            <div>{{ $address->getPersonPhone() }}</div>
        </div>
    @endif
    @if($address->getCountry()->getName() !== '')
        <div>
            <div class="display-address-label">País:</div>
            <div>{{ $address->getCountry()->getName() }}</div>
        </div>
    @endif
    @if($address->getState()->getName() !== '')
        <div>
            <div class="display-address-label">Provincia:</div>
            <div>{{ $address->getState()->getName() }}</div>
        </div>
    @endif
    @if($address->getLocation() !== '')
        <div>
            <div class="display-address-label">Población:</div>
            <div>{{ $address->getLocation() }}</div>
        </div>
    @endif
    @if($address->getAddress() !== '')
        <div>
            <div class="display-address-label">Dirección:</div>
            <div>{{ $address->getAddress() }}</div>
        </div>
    @endif
    @if($address->getPostalCode() !== '')
        <div>
            <div class="display-address-label">Código postal:</div>
            <div>{{ $address->getPostalCode() }}</div>
        </div>
    @endif
</div>
