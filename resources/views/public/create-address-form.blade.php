<form method="POST" action="{{ route('bpanel4-addresses.public.create') }}">
    <div class="row">
        @include('bpanel4-addresses::public.edit-address-fields', [
            'model' => new \Bittacora\Bpanel4\Addresses\Models\ModelAddress(),
            'addressable' => $addressable ?? null,
            'return_route' => $return_route ?? null
        ])
    </div>
    @csrf
    <div class="d-flex justify-content-end">
        <input type="submit" class="btn btn-primary" value="Enviar">
    </div>
</form>
