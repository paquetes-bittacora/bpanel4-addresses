{{--
    Esta vista está pensada para insertarse en el apartado "Mi cuenta" de la ficha de un usuario y similares, es decir
    en páginas "públicas" que necesiten los campos para editar una dirección, pero sin la etiqueta <form>.

    Recibe el parámetro $model que será la dirección a editar.
--}}

@if(!isset($prefix))
    @php
        $prefix = '';
    @endphp
@endif

<div class="col-12 pb-4">
    @livewire('form::input-text', ['fieldWidth' => 9 , 'name' => $prefix . 'name', 'labelText' =>
    __('bpanel4-addresses::address.name'),
    'required'=>true, 'value' => old('name') ?? $model->name, 'placeholder' =>
    __('bpanel4-addresses::address.name-placeholder')], key('name-'. Str::random(10)))
</div>
{{-- Nombre de la persona de contacto --}}
<div class="col-md-6 pb-4">
    @livewire('form::input-text', [
        'fieldWidth' => 9 ,
        'name' => $prefix . 'person_name',
        'labelText' => __('bpanel4-addresses::address.person_name'),
        'required'=>true,
        'value' => old('person_name') ?? $model->person_name,
        'placeholder' => __('bpanel4-addresses::address.person_name_placeholder')
    ], key('person_name-'. Str::random(10)))
</div>
{{-- Apellidos de la persona de contacto --}}
<div class="col-md-6 pb-4">
    @livewire('form::input-text', [
    'fieldWidth' => 9 ,
    'name' => $prefix . 'person_surname',
    'labelText' => __('bpanel4-addresses::address.person_surname'),
    'required'=>true,
    'value' => old('person_surname') ?? $model->person_surname,
    'placeholder' => __('bpanel4-addresses::address.person_surname_placeholder')
    ], key('person_surname-'. Str::random(10)))
</div>
<div class="col-md-6 pb-4">
    @livewire('form::input-text', [
    'fieldWidth' => 9 ,
    'name' => $prefix . 'person_nif',
    'labelText' => __('bpanel4-addresses::address.person_nif'),
    'required'=>true,
    'value' => old('person_nif') ?? $model->person_nif,
    'helpText' => __('bpanel4-addresses::address.person_nif_help'),
    ], key('person_surname-'. Str::random(10)))
</div>
{{-- Teléfono de la persona de contacto --}}
<div class="col-md-6 pb-4">
    @livewire('form::input-text', [
    'fieldWidth' => 9 ,
    'name' => $prefix . 'person_phone',
    'labelText' => __('bpanel4-addresses::address.person_phone'),
    'required'=> false,
    'value' => old('person_phone') ?? $model->person_phone,
    ], key('person_phone-'. Str::random(10)))
</div>
<div class="col-md-6 pb-4">
    <div class="form-group form-row">
        <div class="col-sm-3 col-form-label text-sm-right">
            <label for="id-form-field-1" class="mb-0  ">
                <span class="text-danger">*</span> País
            </label>
        </div>
        <div class="col-sm-9">
            <div class="input-group m-b">
                @livewire('country-select', ['fieldName' => $prefix . 'country', 'selectedCountry' =>
                old('selectedCountry') ??
                $model->country_id ?? 0], key('country-select-'. Str::random(10)))
            </div>
        </div>
    </div>
</div>

<div class="col-md-6 pb-4">
    <div class="form-group form-row">
        <div class="col-sm-3 col-form-label text-sm-right">
            <label for="id-form-field-1" class="mb-0  ">
                <span class="text-danger">*</span> Provincia
            </label>
        </div>
        <div class="col-sm-9">
            <div class="input-group m-b">
                @livewire('state-select', ['fieldName' => $prefix . 'state', 'selectedCountry' => old('selectedCountry') ??
                $model->country_id ?? 0, 'selectedState' => old('selectedState') ?? $model->state_id ?? 0], key('state-select-'.
                Str::random(10)) )
            </div>
        </div>
    </div>
</div>
<div class="col-md-6 pb-4">
    @livewire('form::input-text', ['fieldWidth' => 9 , 'name' => $prefix . 'location', 'labelText' =>
    __('bpanel4-addresses::address.location'), 'required'=>true, 'value' => old('location') ?? $model->location],
    key('location-' . Str::random(10)) )
</div>
<div class="col-md-6 pb-4">
    @livewire('form::input-text', ['fieldWidth' => 9 , 'name' => $prefix . 'address', 'labelText' =>
    __('bpanel4-addresses::address.address'), 'required'=>true, 'value' => old('address') ?? $model->address],
    key('address-' . Str::random(10)))
</div>
<div class="col-md-6 pb-4">
    @livewire('form::input-text', ['fieldWidth' => 9 , 'name' => $prefix . 'postal_code', 'labelText' =>
    __('bpanel4-addresses::address.postal-code'), 'required'=>true, 'value' => old('postal_code') ??
    $model->postal_code], key('postal_code-' . Str::random(10)))
</div>

@if(isset($addressable))
    <input type="hidden" name="addressable_type" value="{{ Crypt::encryptString($addressable::class) }}">
    <input type="hidden" name="addressable_id" value="{{ Crypt::encryptString($addressable->id) }}">
@endif

@if(isset($return_route))
    <input type="hidden" name="return_route" value="{{ $return_route }}">
@endif
