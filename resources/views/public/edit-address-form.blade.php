{{--
    Formulario de edición de una dirección. Recibe el parámetro 'address' que será un objeto de tipo ModelAddress.
--}}
<form method="POST" action="{{ route('bpanel4-addresses.public.update', ['address' => $address]) }}">
    <div class="row">
        @include('bpanel4-addresses::public.edit-address-fields', ['model' => $address])
    </div>
    @csrf
    <input type="hidden" name="_address-token"
           value="{{ \Illuminate\Support\Facades\Crypt::encryptString($address->id) }}">
    <div class="d-flex justify-content-end">
        <input type="submit" class="btn btn-primary" value="Enviar">
    </div>
</form>
