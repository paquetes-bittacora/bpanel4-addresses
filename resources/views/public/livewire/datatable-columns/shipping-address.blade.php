@if ($client->getShippingAddress()->getId() === $row->getId())
    <i class="fas fa-check-square"></i>
@else
    <i class="far fa-check-square" wire:click="setShippingAddress({{ $row->getId() }})" wire:key="shipping-address-{{ $row->getId() }}"></i>
@endif
