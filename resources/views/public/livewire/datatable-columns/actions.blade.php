<a href="{{ route($editAddressRouteName, ['address' => $row->id]) }}" class="edit-address-button"><i class="fa fa-pencil"></i></a>
<form method="POST" class="d-inline mx-2"
      action="{{route('bpanel4-addresses.public.destroy', ['address' => $row->id])}}">
    {!!method_field('DELETE')!!}
    @csrf
    <button id="delete_button{{$model->id}}" type="submit" class="btn btn-primary-outline text-danger"
            onclick="return confirm('¿Está seguro de querer borrar esta dirección?')">
        <i class="fa fa-trash"></i>
    </button>
</form>
