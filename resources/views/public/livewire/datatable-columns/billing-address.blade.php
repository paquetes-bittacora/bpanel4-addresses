<div >
    @if ($client->getBillingAddress()->getId() === $row->getId())
        <i class="fas fa-check-square"></i>
    @else
        @if (null === $row->getNif() || '' === $row->getNif())
            <i class="far fa-check-square" style="cursor: not-allowed; opacity: .5" title="No puede seleccionar esta dirección para facturación porque no tiene NIF/CIF/NIE"></i>
        @else
            <i class="far fa-check-square" wire:click="setBillingAddress({{ $row->getId() }})"></i>
        @endif
    @endif
</div>