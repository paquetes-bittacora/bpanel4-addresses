<div>
    @livewire('bpanel4-addresses::public.addresses-table', [
        'model' => $model,
        'client' => $client,
        'editAddressRouteName' => $editAddressRouteName
    ], key('public-addresses-datatable'))
</div>
