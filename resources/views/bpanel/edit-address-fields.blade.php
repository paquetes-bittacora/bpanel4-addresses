{{-- Vista de edición de dirección en bPanel --}}
@livewire('form::input-text', ['name' => 'address[name]', 'labelText' => __('bpanel4-addresses::address.name'), 'required'=>true, 'value' => old('name') ?? $model->name, 'placeholder' => __('bpanel4-addresses::address.name-placeholder')])
<div class="form-group form-row">
    <div class="col-sm-3 col-form-label text-sm-right">
        <label for="id-form-field-1" class="mb-0  ">
            <span class="text-danger">*</span> {{ __('bpanel4-addresses::address.country') }}
        </label>
    </div>
    <div class="col-sm-7">
        @livewire('country-select', ['selectedCountry' => old('selectedCountry') ?? $model->country_id, 'class' => 'form-control'])
    </div>
</div>
<div class="form-group form-row">
    <div class="col-sm-3 col-form-label text-sm-right">
        <label for="id-form-field-1" class="mb-0  ">
            <span class="text-danger">*</span> {{ __('bpanel4-addresses::address.state') }}
        </label>
    </div>
    <div class="col-sm-7">
        @livewire('state-select', ['selectedCountry' => old('selectedCountry') ?? $model->country_id, 'selectedState' => old('selectedState') ?? $model->state_id, 'class' => 'form-control'])
    </div>
</div>
@livewire('form::input-text', ['name' => 'address[location]', 'labelText' => __('bpanel4-addresses::address.location'), 'required'=>true, 'value' => old('location') ?? $model->location])
@livewire('form::input-text', ['name' => 'address[address]', 'labelText' => __('bpanel4-addresses::address.address'), 'required'=>true, 'value' => old('address.address') ?? $model->address])
@livewire('form::input-text', ['name' => 'address[postal_code]', 'labelText' => __('bpanel4-addresses::address.postal-code'), 'required'=>true, 'value' => old('postal_code') ?? $model->postal_code])
