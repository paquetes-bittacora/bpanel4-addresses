<td>
    {{ $row->name }}
</td>
<td>
    {{ $row->country->name }}
</td>
<td>
    {{ $row->state->name }}
</td>
<td>
    {{ $row->location }}
</td>
<td>
    {{ $row->postal_code }}
</td>
<td>
    @livewire('utils::datatable-poly-default', [
        'fieldName' => 'main',
        'model' => $row,
        'value' => $row->main,
        'size' => 'xxs',
        'relationName' => 'addressable',
        'reloadRouteName' => Illuminate\Support\Facades\Route::currentRouteName(),
        'reloadRouteParams' => (Illuminate\Support\Facades\Route::getCurrentRoute())->originalParameters()
    ], key('main-address-'.$row->id))
</td>
<td>
    <a href="{{ route($editAddressRouteName, ['address' => $row->id]) }}" class="edit-address-button"><i class="fa fa-pencil"></i></a>
    <form method="POST" class="d-inline mx-2"
          action="{{route('bpanel4-addresses.public.destroy', ['address' => $row->id])}}">
        {!!method_field('DELETE')!!}
        @csrf
        <button id="delete_button{{$model->id}}" type="submit" class="btn btn-primary-outline text-danger"
                onclick="return confirm('¿Está seguro de querer borrar esta dirección?')">
            <i class="fa fa-trash"></i>
        </button>
    </form>
</td>

@pushonce('scripts')
<script>
    document.querySelector('a[wire\\:click\\.prevent="bulkDelete"]').addEventListener('click', function(event) {
      if (!window.confirm('¿Seguro que desea eliminar los elementos seleccionados?')) {
        event.preventDefault();
        event.stopPropagation();
        return false;
      }
    }, true);
</script>
@endpushonce
