<form class="mt-3 mb-0" method="POST" action="{{ route('bpanel4-addresses.bpanel.update', ['address' => $address]) }}">
    @include('bpanel4-addresses::bpanel.edit-address-fields', ['model' => $address])
    @csrf
    <input type="hidden" name="_address-token" value="{{ \Illuminate\Support\Facades\Crypt::encryptString($address->id) }}">
    <div class="col-12 mt-5 border-t-1 bgc-secondary-l4 brc-secondary-l2 py-35 d-flex justify-content-center">
        @livewire('form::save-button',['theme'=>'update'])
        @livewire('form::save-button',['theme'=>'reset'])
    </div>
</form>
