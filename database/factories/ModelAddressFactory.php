<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Addresses\Database\Factories;

use Bittacora\Bpanel4\Orders\Database\Factories\TestClientFactory;
use Bittacora\Bpanel4\Orders\Tests\Feature\Models\TestClient;
use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Bittacora\LivewireCountryStateSelector\Database\Factories\CountryFactory;
use Bittacora\LivewireCountryStateSelector\Database\Factories\StateFactory;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @method ModelAddress createOne($attributes = [])
 * @extends Factory<ModelAddress>
 */
final class ModelAddressFactory extends Factory
{
    /** @var string  */
    protected $model = ModelAddress::class;

    /**
     * @return array<string, int>|array<string, string>|array<string, mixed[]>
     */
    public function definition(): array
    {
        $country = (new CountryFactory())->createOne();
        return [
            'name' => $this->faker->words(asText: true),
            'country_id' => $country->getId(),
            'postal_code'=> $this->faker->postcode(),
            'location' => $this->faker->city(),
            'address' => $this->faker->words(4, true),
            'state_id' => (new StateFactory())->createOne(),
            'addressable_id' => (new TestClientFactory())->createOne(),
            'addressable_type' => TestClient::class,
            'person_name' => $this->faker->name(),
            'person_surname' => $this->faker->lastName(),
            'person_nif' => $this->faker->randomNumber(9, true) . $this->faker->randomLetter(),
        ];
    }
}
