<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::table('model_addresses', static function (Blueprint $table) {
            $table->boolean('main')->default(false);

            // No añadimos un índice único para main porque si no no podría haber 2 direcciones con 'main' a 0. El que
            // solo haya una dirección marcada como 'main' debe controlarse por código.
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::table('model_addresses', function (Blueprint $table) {
            $table->dropColumn('main');
        });
    }
};
