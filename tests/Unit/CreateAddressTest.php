<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Addresses\Tests\Unit;

use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Bittacora\Bpanel4\Addresses\UseCases\CreateAddress;
use Bittacora\Bpanel4\Clients\Models\Client;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

final class CreateAddressTest extends TestCase
{
    use RefreshDatabase;

    public function testSePuedeCrearUnaDireccionSinPersonNameYSurname(): void
    {
        // Arrange

        // Act
        $createAddress = new CreateAddress();
        $result = $createAddress->handle([
            'country' => 1,
            'state' => 1,
            'address' => [
                'name' => 'Address name',
                'address' => 'abc',
                'location' => 'Address location',
                'postal_code' => '01001',
            ]
        ],
            Client::class,
            1
        );

        // Assert
        $this->assertInstanceOf(ModelAddress::class, $result);
    }
}