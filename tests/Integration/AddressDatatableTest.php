<?php

namespace Bittacora\Bpanel4\Addresses\Tests\Integration;

use Bittacora\Bpanel4\Addresses\Database\Factories\ModelAddressFactory;
use Bittacora\Bpanel4\Addresses\Http\Livewire\AddressesDatatable;
use Bittacora\Bpanel4\Clients\Database\Factories\ClientFactory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Livewire\Livewire;
use Tests\TestCase;

class AddressDatatableTest extends TestCase
{
    use RefreshDatabase;

    public function testSePuedeCrearElComponente(): void
    {
        $component = $this->getComponent();
        $component->assertOk();
    }

    public function testActualizaLaDireccionDeEnvio(): void
    {
        $component = $this->getComponent();
        $component->call('setShippingAddress', (new ModelAddressFactory())->createOne());
        $component->assertDispatchedBrowserEvent('address-updated');
    }

    public function testActualizaLaDireccionDeFacturacion(): void
    {
        $component = $this->getComponent();
        $component->call('setBillingAddress', (new ModelAddressFactory())->createOne());
        $component->assertDispatchedBrowserEvent('address-updated');
    }

    /**
     * @return \Livewire\Testing\TestableLivewire&AddressesDatatable
     */
    private function getComponent(): \Livewire\Testing\TestableLivewire
    {
        $client = (new ClientFactory())->createOne();
        $this->actingAs($client->getUser());
        $component = Livewire::test(AddressesDatatable::class, [
            'client' => $client,
            'model' => (new ModelAddressFactory())->createOne()
        ]);
        return $component;
    }
}
