<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Addresses\Tests\Integration;

use Bittacora\Bpanel4\Addresses\Database\Factories\ModelAddressFactory;
use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Bittacora\Bpanel4\Clients\Database\Factories\ClientFactory;
use Bittacora\Bpanel4\Clients\Models\Client;
use Bittacora\Bpanel4\Products\Tests\Acceptance\SetsUpApplication;
use Bittacora\Bpanel4Users\Models\User;
use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Crypt;
use Random\RandomException;
use Tests\TestCase;

final class PublicAddressControllerTest extends TestCase
{
    use RefreshDatabase;
    use SetsUpApplication;

    private UrlGenerator $urlGenerator;

    protected function setUp(): void
    {
        parent::setUp();
        $this->urlGenerator = $this->app->make(UrlGenerator::class);
        $this->createCountryAndState();
    }

    /**
     * @throws RandomException
     */
    public function testSePuedeCrearUnaDireccion(): void
    {
        // Arrange
        $addressName = 'Dirección ' . random_int(1, PHP_INT_MAX);
        $client = (new ClientFactory())->createOne();
        $uri = 'https://prueba.com/mis-direcciones';

        // Act
        $result = $this->post($this->urlGenerator->route(
            'bpanel4-addresses.public.create',
            $this->getRequestData(addressName: $addressName, client: $client, uri: $uri)
        ));

        // Assert
        $result->assertRedirect($uri);
        $this->assertDatabaseHas(ModelAddress::class, ['name' => $addressName]);
    }

    /**
     * @throws RandomException
     */
    public function testSeGuardaElDniAlCrearUnaDireccion(): void
    {
        // Arrange
        $requestData = $this->getRequestData();

        // Act
        $this->post($this->urlGenerator->route('bpanel4-addresses.public.create', $requestData));

        // Assert
        $this->assertDatabaseHas(ModelAddress::class, ['person_nif' => $requestData['person_nif']]);
    }

    /**
     * @throws RandomException
     */
    public function testSePuedeEditarUnaDireccion(): void
    {
        // Arrange
        $address = (new ModelAddressFactory())->createOne();
        $requestData = $this->getRequestData();
        $requestData['_address-token'] = Crypt::encrypt($address->id);
        $this->actingAs(User::whereId($address->addressable_id)->firstOrFail());

        // Act
        $this->post(
            $this->urlGenerator->route('bpanel4-addresses.public.update', ['address' => $address->id]),
            $requestData
        );

        // Assert
        $this->assertDatabaseHas(ModelAddress::class, [
            'name' => $requestData['name'],
            'person_nif' => $requestData['person_nif'],
        ]);
    }

    /**
     * @return array<string, int|string>
     * @throws RandomException
     */
    private function getRequestData(
        ?string $addressName = null,
        ?Client $client = null,
        ?string $uri = null,
        ?string $personNif = null,
    ): array {
        if (null === $client) {
            $client = (new ClientFactory())->createOne();
        }

        return [
            'name' => $addressName ?? 'Nombre de la dirección',
            'person_name' => 'Persona de contacto',
            'person_surname' => 'Apellidos Pers. contacto',
            'person_phone' => '',
            'country' => 1,
            'state' => 1,
            'location' => 'Prueba',
            'address' => 'Dirección de prueba',
            'postal_code' => '00000',
            'addressable_type' => Crypt::encryptString($client::class),
            'addressable_id' => Crypt::encryptString((string)$client->getClientId()),
            'return_route' => $uri ?? 'https://prueba.com/mis-direcciones',
            'person_nif' => $personNif ?? random_int(11111111, 99999999) . chr(random_int(65, 90)),
        ];
    }
}
