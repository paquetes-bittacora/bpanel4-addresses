El modelo que vaya a tener direcciones debe usar el trait `ModelWithAddressesTrait`:

```php
use ModelWithAddressesTrait; 
```

(Pendiente de documentar)

## Componente de blade

El paquete registra un componente de Blade que muestra todos los campos de un
objeto ModelAddress.

```
<x-bpanel4-address :address="$address" :showName="true"/>
```

Con la opción `showName` se puede mostrar u ocultar el nombre de la dirección.
