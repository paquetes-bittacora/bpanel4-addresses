<?php

declare(strict_types=1);

use Bittacora\Bpanel4\Addresses\Http\Controllers\BpanelAddressController;
use Bittacora\Bpanel4\Addresses\Http\Controllers\PublicAddressController;

Route::prefix('direcciones')->name('bpanel4-addresses.public.')->middleware(['web'])->group(function () {
    Route::post('/crear', [PublicAddressController::class, 'create'])->name('create');
    Route::post('/{address}/actualizar', [PublicAddressController::class, 'update'])->name('update');
    Route::delete('/{address}/elimniar', [PublicAddressController::class, 'destroy'])->name('destroy');
});

Route::prefix('bpanel/direcciones')->name('bpanel4-addresses.bpanel.')->middleware(['web'])->group(function () {
    Route::post('/{address}/actualizar', [BpanelAddressController::class, 'update'])->name('update');
    Route::delete('/{address}/elimniar', [PublicAddressController::class, 'destroy'])->name('destroy');
});
