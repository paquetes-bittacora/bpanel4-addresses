<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Addresses\UseCases;

use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Bittacora\Bpanel4\Addresses\Validation\AddressValidator;
use Illuminate\Validation\ValidationException;

final class UpdateAddress
{
    public function __construct(private readonly AddressValidator $addressValidator)
    {
    }

    /**
     * @param array<string, string> $data
     * @throws ValidationException
     */
    public function execute(ModelAddress $address, array $data): void
    {
        $this->addressValidator->validateUpdateData($data);

        $data = $this->renameFields($data);

        $address->fill($data);
        $address->save();
    }

    /**
     * @param array<string, string> $data
     * @return array<string, string>
     */
    private function renameFields(array $data): array
    {
        $data['country_id'] = $data['country'];
        $data['state_id'] = $data['state'];
        unset($data['country'], $data['state']);
        return $data;
    }
}
