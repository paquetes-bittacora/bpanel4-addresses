<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Addresses\UseCases;

use Bittacora\Bpanel4\Addresses\Models\ModelAddress;

final class CreateAddress
{
    /**
     * @param array<string, string> $input
     */
    public function handle(array $input, string $addressableType, int $addressableId): ModelAddress
    {
        return ModelAddress::create([
            'country_id' => $input['country'],
            'state_id' => $input['state'],
            'name' => $input['address']['name'],
            'person_name' => $input['address']['person_name'] ?? null,
            'person_surname' => $input['address']['person_surname'] ?? null,
            'person_phone' => $input['address']['person_phone'] ?? null,
            'address' => $input['address']['address'],
            'location' => $input['address']['location'],
            'postal_code' => $input['address']['postal_code'],
            'addressable_type' => $addressableType,
            'addressable_id' => $addressableId,
            'person_nif' => $this->getNif($input),
        ]);
    }

    /**
     * Parche para aceptar DNIs desde distintos formularios
     * @param array<string, string> $input
     */
    private function getNif(array $input): ?string
    {
        return $input['dni'] ?? $input['person_nif'] ?? null;
    }
}
