<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Addresses\Validation;

use Illuminate\Validation\Factory;
use Illuminate\Validation\ValidationException;

final class AddressValidator
{
    public function __construct(private readonly Factory $validator)
    {
    }

    /**
     * Método que valida los datos en casos de uso, etc.
     * @param array<string, mixed> $data
     * @throws ValidationException
     */
    public function validateUpdateData(array $data): void
    {
        $this->validator->make($data, $this->getAddressValidationFields())->validate();
    }

    /**
     * Devuelve las validaciones para campo de la dirección.
     * @return array<string, mixed[]>
     */
    public function getAddressValidationFields(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'country' => ['required'],
            'state' => ['required'],
            'location' => ['required', 'string', 'max:255'],
            'address' => ['required', 'string', 'max:255'],
            'postal_code' => ['required', 'numeric', 'max:999999', 'min:0'],
            'addressable_type' => ['sometimes', 'string'],
            'addressable_id' => ['sometimes', 'string'],
            'return_route' => ['sometimes', 'string'],
        ];
    }
}
