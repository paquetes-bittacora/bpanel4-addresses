<?php

namespace Bittacora\Bpanel4\Addresses\Traits;

use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait ModelWithAddressesTrait
{
    public function addresses(): MorphMany
    {
        return $this->morphMany(ModelAddress::class, 'addressable');
    }
}
