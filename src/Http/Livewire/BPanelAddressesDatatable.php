<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Addresses\Http\Livewire;

use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

final class BPanelAddressesDatatable extends DataTableComponent
{
    /** @var array<int>  */
    public array $selectedKeys;
    public Model $model;

    public string $editAddressRouteName = '';
    /**
     * @return array<Column>
     */
    public function columns(): array
    {
        return [
            Column::make(__('bpanel4-addresses::address.name'), 'name')->addClass("w-30"),
            Column::make(__('bpanel4-addresses::address.country'), 'country')->addClass("w-30"),
            Column::make(__('bpanel4-addresses::address.state'), 'state')->addClass("w-30"),
            Column::make(__('bpanel4-addresses::address.location'), 'location')->addClass("w-30"),
            Column::make(__('bpanel4-addresses::address.postal-code'), 'postal_code')->addClass("w-30"),
            Column::make(__('bpanel4-addresses::address.main'), 'main')->addClass("w-30"),
            Column::make('Acciones')->addClass("w-10 text-center"),
        ];
    }

    /**
     * @return Builder<ModelAddress>
     */
    public function query(): Builder
    {
        /** @var Builder<ModelAddress> */
        return ModelAddress::query()
            ->where('addressable_id', $this->model->id)
            ->where('addressable_type', $this->model::class)
            ->orderBy('created_at', 'DESC')
            ->when($this->getFilter('search'), fn ($query, $term) => $query
                ->where('name', 'like', '%' . $term . '%')
                ->orWhereRelation('user', 'email', 'like', '%' . $term . '%')
                ->orWhereRelation('mainAddress', 'location', 'like', '%' . $term . '%'));
    }

    public function rowView(): string
    {
        return 'bpanel4-addresses::bpanel.livewire.addresses-datatable';
    }

    /**
     * @return string[]
     */
    public function bulkActions(): array
    {
        return [
            'bulkDelete' => 'Eliminar',
        ];
    }

    public function bulkDelete(): void
    {
        if ([] !== $this->selectedKeys()) {
            ModelAddress::destroy($this->selectedKeys);
            $this->resetAll();
        }
    }
}
