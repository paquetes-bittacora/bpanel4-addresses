<?php

/** @noinspection PhpMissingFieldTypeInspection */

declare(strict_types=1);

namespace Bittacora\Bpanel4\Addresses\Http\Livewire;

use Bittacora\Bpanel4\Clients\Contracts\Client;
use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use RuntimeException;

/**
 * @property int[] $selectedKeys
 */
final class AddressesDatatable extends DataTableComponent
{
    /** @var ModelAddress $model */
    public $model;

    /** @var Client $client */
    public $client;

    public string $editAddressRouteName = '';

    /**
     * @return array<Column>
     */
    public function columns(): array
    {
        return [
            Column::make(__('bpanel4-addresses::address.name'), 'name'),
            Column::make(__('bpanel4-addresses::address.country'), 'country.name'),
            Column::make(__('bpanel4-addresses::address.state'), 'state.name'),
            Column::make(__('bpanel4-addresses::address.location'), 'location'),
            Column::make(__('bpanel4-addresses::address.postal-code'), 'postal_code'),
            Column::make(__('bpanel4-addresses::address.shipping-address'), 'id')
                ->format(
                    fn($value, ModelAddress $row, Column $column) => view('bpanel4-addresses::public.livewire.datatable-columns.shipping-address')
                    ->with(['row' => $row, 'client' => $this->client])
                ),
            Column::make(__('bpanel4-addresses::address.billing-address'), 'id')
                ->format(
                    fn($value, ModelAddress $row, Column $column) => view('bpanel4-addresses::public.livewire.datatable-columns.billing-address')
                        ->with(['row' => $row, 'client' => $this->client])
                ),
            Column::make('Acciones', 'id')->format(
                fn($value, ModelAddress $row, Column $column) => view('bpanel4-addresses::public.livewire.datatable-columns.actions')
                    ->with(['editAddressRouteName' => $this->editAddressRouteName, 'row' => $row, 'model' => $this->model])
            ),
        ];
    }

    /**
     * @return Builder<ModelAddress>
     */
    public function query(): Builder
    {
        $builder = ModelAddress::query()->where('addressable_id', $this->client->getClientId())
            ->select('person_nif')
            ->where('addressable_type', $this->model::class)
            ->orderBy('model_addresses.created_at', 'DESC')
            ->when(
                $this->getAppliedFilterWithValue('search'),
                fn ($query, $term) => $query->where('name', 'like', '%' . $term . '%')
                ->orWhereRelation('user', 'email', 'like', '%' . $term . '%')
                ->orWhereRelation('mainAddress', 'location', 'like', '%' . $term . '%')
            );

        if (!$builder instanceof Builder) {
            throw new RuntimeException('Ocurrió un error al crear la tabla de direcciones');
        }

        return $builder;
    }

    public function rowView(): string
    {
        return 'bpanel4-addresses::public.livewire.addresses-datatable';
    }

    public function setShippingAddress(ModelAddress $shippingAddress): void
    {
        $this->client->setShippingAddress($shippingAddress);
        $this->dispatchBrowserEvent('address-updated', ['message' => 'Dirección de envío actualizada']);
    }

    public function setBillingAddress(ModelAddress $billingAddress): void
    {
        $this->client->setBillingAddress($billingAddress);
        $this->dispatchBrowserEvent('address-updated', ['message' => 'Dirección de facturación actualizada']);
    }

    public function configure(): void
    {
        $this->setPrimaryKey('id');
        $this->setColumnSelectDisabled();
    }

    public function builder(): Builder
    {
        return $this->query();
    }
}
