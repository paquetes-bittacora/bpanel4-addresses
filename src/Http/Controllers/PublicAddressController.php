<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Addresses\Http\Controllers;

use App\Http\Controllers\Controller;
use Bittacora\Bpanel4\Addresses\Http\Requests\CreateAddressRequest;
use Bittacora\Bpanel4\Addresses\Http\Requests\UpdateAddressRequest;
use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Bittacora\Bpanel4\Addresses\UseCases\CreateAddress;
use Bittacora\Bpanel4\Addresses\UseCases\UpdateAddress;
use Bittacora\Bpanel4\Clients\Models\Client;
use Exception;
use Illuminate\Auth\AuthManager;
use Illuminate\Encryption\Encrypter;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Validation\ValidationException;
use RuntimeException;

final class PublicAddressController extends Controller
{
    public function __construct(
        private readonly Redirector $redirector,
        private readonly Encrypter $crypt,
        private readonly AuthManager $authManager,
    ) {
    }

    public function create(CreateAddressRequest $request, CreateAddress $createAddress): RedirectResponse
    {
        try {
            $addressableType = Crypt::decryptString($request->input('addressable_type'));
            $addressableId = (int) Crypt::decryptString($request->input('addressable_id'));
            $returnRoute = $request->input('return_route', null);
            $createAddress->handle(
                $this->formatAddressCreationArray($request),
                $addressableType,
                $addressableId
            );
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception) {
            return $this->redirector->back()->with('alert-danger', 'Error al crear la dirección');
        }

        if (null === $returnRoute) {
            return $this->redirector->back()->with('alert-success', 'Dirección creada');
        }

        return $this->redirector->to($returnRoute)->with('alert-success', 'Dirección creada');
    }

    public function update(
        ModelAddress $address,
        UpdateAddressRequest $request,
        UpdateAddress $updateAddress
    ): RedirectResponse {
        $this->validateUserPermission($address);
        $this->validateAddressToken($address, $request);
        try {
            $updateAddress->execute($address, $request->except(['_token', '_address-token', 'return_route']));
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception) {
            return $this->redirector->back()->with('alert-danger', 'Error al actualizar la dirección');
        }

        return $this->redirector->back()->with('alert-success', 'Dirección actualizada');
    }

    public function destroy(ModelAddress $address): RedirectResponse
    {
        $this->validateUserPermission($address);
        $address->delete();
        return $this->redirector->back()->with('alert-success', 'Dirección eliminada');
    }

    private function validateUserPermission(ModelAddress $address): void
    {
        $userId = $this->authManager->user()->id;

        $clientId = Client::select('id')->where('user_id', $userId)->firstOrFail()->id;

        if ($clientId !== $address->addressable_id) {
            abort(403);
        }
    }

    private function validateAddressToken(ModelAddress $address, UpdateAddressRequest $request): void
    {
        $addressToken = $request->request->get('_address-token');
        $decryptedId = $this->crypt->decryptString((string)$addressToken);
        if (
            ($address->id !== (int)$decryptedId) &&
            ($address->id !== (int)unserialize($decryptedId, ['allowed_classes' => false]))
        ) {
            // El id que viene por la URL es distinto del id de la dirección para la que se creó el formulario, se ha
            // modificado el id en la URL.
            throw new RuntimeException('Token de dirección no válido');
        }
    }

    private function formatAddressCreationArray(CreateAddressRequest $request): array
    {
        $data = $request->except(['_token', '_address-token', 'addressable_type', 'addressable_id', 'return_route']);
        $address = $data['address'];
        $data['address'] = [];
        $data['address']['name'] = $data['name'];
        $data['address']['person_name'] = $data['person_name'];
        $data['address']['person_surname'] = $data['person_surname'];
        $data['address']['person_phone'] = $data['person_phone'];
        $data['address']['address'] = $address;
        $data['address']['location'] = $data['location'];
        $data['address']['postal_code'] = $data['postal_code'];
        unset($data['name']);
        unset($data['person_name']);
        unset($data['person_surname']);
        unset($data['person_phone']);
        unset($data['location']);
        unset($data['postal_code']);
        return $data;
    }
}
