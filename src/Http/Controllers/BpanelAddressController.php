<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Addresses\Http\Controllers;

use Bittacora\Bpanel4\Addresses\Http\Requests\UpdateAddressRequest;
use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Bittacora\Bpanel4\Addresses\UseCases\UpdateAddress;
use Exception;
use Illuminate\Encryption\Encrypter;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Validation\ValidationException;
use RuntimeException;

final class BpanelAddressController
{
    public function __construct(
        private readonly Redirector $redirector,
        private readonly Encrypter $crypt
    ) {
    }

    /**
     * @throws ValidationException
     */
    public function update(
        ModelAddress $address,
        UpdateAddressRequest $request,
        UpdateAddress $updateAddress
    ): RedirectResponse {
        $this->validateAddressToken($address, $request);
        try {
            $updateAddress->execute($address, $request->request->all());
        } catch (ValidationException $exception) {
            throw $exception;
        } catch (Exception) {
            return $this->redirector->back()->with('error', 'Error al actualizar la dirección');
        }

        return $this->redirector->back()->with('alert-success', 'Dirección actualizada');
    }

    public function destroy(ModelAddress $address): RedirectResponse
    {
        $address->delete();
        return $this->redirector->back()->with('success', 'Dirección eliminada');
    }

    private function validateAddressToken(ModelAddress $address, UpdateAddressRequest $request): void
    {
        if ($address->id !== (int)$this->crypt->decryptString($request->request->get('_address-token'))) {
            // El id que viene por la URL es distinto del id de la dirección para la que se creó el formulario, se ha
            // modificado el id en la URL.
            throw new RuntimeException('Token de dirección no válido');
        }
    }
}
