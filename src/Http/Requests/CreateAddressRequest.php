<?php

namespace Bittacora\Bpanel4\Addresses\Http\Requests;

use Bittacora\Bpanel4\Addresses\Validation\AddressValidator;
use Illuminate\Foundation\Http\FormRequest;

final class CreateAddressRequest extends FormRequest
{
    public function __construct(private readonly AddressValidator $addressValidator)
    {
    }

    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        $rules = $this->addressValidator->getAddressValidationFields();

        return $rules;
    }
}
