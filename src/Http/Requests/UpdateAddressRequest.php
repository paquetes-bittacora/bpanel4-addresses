<?php

namespace Bittacora\Bpanel4\Addresses\Http\Requests;

use Bittacora\Bpanel4\Addresses\Validation\AddressValidator;
use Illuminate\Foundation\Http\FormRequest;

final class UpdateAddressRequest extends FormRequest
{
    public function __construct(private readonly AddressValidator $addressValidator)
    {
    }

    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        $rules = $this->addressValidator->getAddressValidationFields();
        $rules['_address-token'] = 'required|string';

        return $rules;
    }
}
