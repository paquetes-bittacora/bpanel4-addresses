<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Addresses;

use Bittacora\Bpanel4\Addresses\Http\Livewire\AddressesDatatable;
use Bittacora\Bpanel4\Addresses\Http\Livewire\BPanelAddressesDatatable;
use Bittacora\Bpanel4\Addresses\View\Components\Address;
use Blade;
use Illuminate\Support\ServiceProvider;
use Livewire;

final class Bpanel4AddressesServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
        $this->loadTranslationsFrom(__DIR__ . '/../resources/lang', 'bpanel4-addresses');
        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'bpanel4-addresses');
        $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');

        Livewire::component('bpanel4-addresses::public.addresses-table', AddressesDatatable::class);
        Livewire::component('bpanel4-addresses::bpanel.addresses-table', BPanelAddressesDatatable::class);

        Blade::component(Address::class, 'bpanel4-address');
    }
}
