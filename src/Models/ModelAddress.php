<?php

namespace Bittacora\Bpanel4\Addresses\Models;

use Bittacora\Bpanel4\Addresses\Exceptions\CountryNotFoundException;
use Bittacora\Bpanel4\Addresses\Exceptions\StateNotFoundException;
use Bittacora\LivewireCountryStateSelector\Models\Country;
use Bittacora\LivewireCountryStateSelector\Models\State;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\ItemNotFoundException;

/**
 * \Bittacora\Bpanel4\Addresses\Models\ModelAddress
 *
 * @property int $id
 * @property int $client_id
 * @property int $country_id
 * @property int|null $state_id
 * @property string $postal_code
 * @property string $location
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|ModelAddress newModelQuery()
 * @method static Builder|ModelAddress newQuery()
 * @method static Builder|ModelAddress query()
 * @method static Builder|ModelAddress whereClientId($value)
 * @method static Builder|ModelAddress whereCountryId($value)
 * @method static Builder|ModelAddress whereCreatedAt($value)
 * @method static Builder|ModelAddress whereId($value)
 * @method static Builder|ModelAddress whereLocation($value)
 * @method static Builder|ModelAddress wherePostalCode($value)
 * @method static Builder|ModelAddress whereStateId($value)
 * @method static Builder|ModelAddress whereUpdatedAt($value)
 * @property string $addressable_type
 * @property int $addressable_id
 * @property int $main
 * @property string|null $name
 * @property string|null $address
 * @property-read Model|\Eloquent $addressable
 * @property-read Country|null $country
 * @property-read State|null $state
 * @method static Builder|ModelAddress whereAddress($value)
 * @method static Builder|ModelAddress whereAddressableId($value)
 * @method static Builder|ModelAddress whereAddressableType($value)
 * @method static Builder|ModelAddress whereMain($value)
 * @mixin \Eloquent
 */
final class ModelAddress extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * @var string[]
     */
    protected $fillable = [
        'client_id',
        'country_id',
        'state_id',
        'location',
        'postal_code',
        'main',
        'person_name',
        'person_surname',
        'person_phone',
        'address',
        'name',
        'addressable_id',
        'addressable_type',
        'person_nif',
    ];

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getPersonName(): string
    {
        return $this->person_name ?? '';
    }

    public function getPersonSurname(): string
    {
        return $this->person_surname ?? '';
    }

    public function getPersonPhone(): string
    {
        return $this->person_phone ?? '';
    }

    /** @return MorphTo<Model, ModelAddress> */
    public function addressable(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * @return BelongsTo<Country, ModelAddress>
     */
    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class);
    }

    /**
     * @return BelongsTo<State, ModelAddress>
     */
    public function state(): BelongsTo
    {
        return $this->belongsTo(State::class);
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getLocation(): string
    {
        return $this->location;
    }

    public function getPostalCode(): string
    {
        return $this->postal_code;
    }

    public function getAddress(): string
    {
        return $this->address ?? '';
    }

    public function getCountry(): Country
    {
        try {
            return $this->country()->get()->firstOrFail();
        } catch (ItemNotFoundException) {
            $countryNotFoundException = new CountryNotFoundException();
            $countryNotFoundException->setAddress($this);
            throw $countryNotFoundException;
        }
    }

    public function getState(): State
    {
        try {
            return $this->state()->get()->firstOrFail();
        } catch (ItemNotFoundException) {
            $stateNotFoundException = new StateNotFoundException();
            $stateNotFoundException->setAddress($this);
            throw $stateNotFoundException;
        }
    }

    public function getNif(): ?string
    {
        return $this->person_nif;
    }
}
