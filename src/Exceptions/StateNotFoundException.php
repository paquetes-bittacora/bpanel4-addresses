<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Addresses\Exceptions;

use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Exception;

final class StateNotFoundException extends Exception
{
    private ModelAddress $address;

    public function getAddress(): ModelAddress
    {
        return $this->address;
    }

    public function setAddress(ModelAddress $address): void
    {
        $this->address = $address;
    }
}