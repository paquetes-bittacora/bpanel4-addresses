<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Addresses\View\Components;

use Bittacora\Bpanel4\Addresses\Models\ModelAddress;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

final class Address extends Component
{
    /**
     * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
     */
    public function __construct(
        private readonly Factory $view,
        public readonly ModelAddress $address,
        public readonly bool $showName = false,
    ) {
    }

    public function render(): View
    {
        return $this->view->make('bpanel4-addresses::components.address');
    }
}
